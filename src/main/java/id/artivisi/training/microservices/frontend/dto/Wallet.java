package id.artivisi.training.microservices.frontend.dto;

import lombok.Data;

@Data
public class Wallet {
    private String id;
    private String idUser;
    private String name;
}
