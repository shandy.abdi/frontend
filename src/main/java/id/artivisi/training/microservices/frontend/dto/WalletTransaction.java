package id.artivisi.training.microservices.frontend.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class WalletTransaction {
    private Wallet wallet;
    private LocalDateTime transactionTime = LocalDateTime.now();
    private BigDecimal amount;
    private String description;
    private String reference;
}
